class ApplicationController < ActionController::Base
  def current_page
    @current_page = params[:page]&.to_i || 1
  end
end
