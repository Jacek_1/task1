class BookmarksController < ApplicationController
  require 'open-uri'
  PER_PAGE = 5
  before_action :bookmarks, only: :index
  respond_to :html
  respond_to :js, only: [:index]

  def index
    respond_to do |format|
      format.html
      format.js { respond_with @grouped_bookmarks }
    end
  end

  def show
    @bookmark = Bookmark.find(params[:id])
  end

  def create
    begin
      setup_bookmark
    rescue
      flash[:error] = 'Invalid url or tags'
    else
      flash[:error] = nil
      flash[:message] = 'Succesfully added bookmark'
    end
    redirect_to method: index
  end

  private
  def bookmarks
    @bookmarks = search.page(current_page).per(PER_PAGE).order('domain_id')
    @grouped_bookmarks = @bookmarks.group_by(&:domain_id)
    @domain = params[:last_domain]
  end

  def setup_bookmark
    page = Nokogiri::HTML(open(params[:bookmark][:url]))
    parsed_url = URI.parse(params[:bookmark][:url])
    host, url = parsed_url.host, parsed_url.to_s
    if (domain = Domain.find_by_domain(host)).nil?
      domain = Domain.create!(domain: host)
    end
    @bookmark = Bookmark.create!(
      url: url,
      domain_id: domain.id,
      title: page.title,
      description: page.at("head meta[name='description']")["content"],
      tags: URI.escape(params[:bookmark][:tags]).gsub(/\W+/, ',')
    )
  end

  def search
    if name_search
      quoted_name = "%#{params[:name_search].gsub('_', '\\_')}%"
      bookmark = Bookmark.having('title LIKE ? OR url LIKE ? OR tags like ? OR description like ?', quoted_name, quoted_name, quoted_name, quoted_name)
    else
      bookmark = Bookmark
    end
    bookmark
  end

  def name_search
    @name_search = params[:name_search] unless params[:name_search]&.empty?
  end

end
