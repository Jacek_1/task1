class DomainsController < ApplicationController
  PER_PAGE = 5
  respond_to :js, only: [:index]

  def index
    respond_to do |format|
      format.html
      format.js { respond_with bookmarks }
    end
  end

  def show
    bookmarks
  end

  private
  def bookmarks
    @bookmarks = Bookmark.where(domain_id: params[:id]).page(current_page).per(PER_PAGE)
  end
end
