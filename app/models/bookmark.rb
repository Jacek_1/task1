class Bookmark < ApplicationRecord
  belongs_to :domain
  validates :tags, presence: true
  validates :url, uniqueness: true
end
