class Domain < ApplicationRecord
  has_many :bookmarks, dependent: :destroy
end
