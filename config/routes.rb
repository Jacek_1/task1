Rails.application.routes.draw do
  resources 'bookmarks'
  resources 'domains'
end
