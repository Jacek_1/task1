class CreateDomains < ActiveRecord::Migration[5.2]
  def change
    create_table :domains do |t|
      t.string :domain

      t.timestamps
    end
    add_index :domains, :domain
  end
end
