class CreateBookmarks < ActiveRecord::Migration[5.2]
  def change
    create_table :bookmarks do |t|
      t.references :domain, foreign_key: true
      t.string :url
      t.string :title
      t.string :tags
      t.string :description
      t.string :minimizeURL

      t.timestamps
    end
    add_index :bookmarks, :url
  end
end
